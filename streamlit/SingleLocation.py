import folium
import streamlit as st
from streamlit_folium import st_folium
from folium.plugins import MarkerCluster
from folium.plugins import FastMarkerCluster
from folium.plugins import TimestampedGeoJson

import geopandas as gpd
from pathlib import Path
import pandas as pd
from shapely.geometry import Point, Polygon
from datetime import date, datetime, timedelta
from folium.plugins import HeatMap
from folium.plugins import Draw

def createDf(geodf):
    geodf['geometry'] = geodf['geometry'].apply(lambda geom: str(geom))
    st.dataframe(geodf)


# loading data
@st.cache_data
def loaddata():
    path = Path(__file__).parent / "../assets/felids_meta.csv"
    df = pd.read_csv(path, index_col='id', parse_dates=True)
    geometry = [Point(xy) for xy in zip(df['long'], df['lat'])]
    df=df[['observed_on', 'time', ]]
    df.time = pd.to_datetime(df.time, format='mixed')
    gdf = gpd.GeoDataFrame(df, geometry=geometry, crs='EPSG:4326')
    return gdf
gdf = loaddata()

# settings
with st.form("my_form"):
    mindate = gdf.time.min().date()
    maxdate = gdf.time.max().date()
    dateFrom, dateTo = st.date_input(
        "Select dateRange",
        (maxdate-timedelta(weeks=4), maxdate),
        mindate,
        maxdate,
        format="DD.MM.YYYY",
    )

    st.caption('select a location')
    provider = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
    m3 = folium.Map(tiles=provider, attr="osm", location=(0,0), start_zoom=2)
    Draw(export=False, show_geometry_on_click=True).add_to(m3)

    data = st_folium(m3, width=1500, returned_objects=["last_active_drawing"] )
    submitted = st.form_submit_button("Submit")
polygon = Polygon(data['last_active_drawing']['geometry']['coordinates'][0])



gdf = gdf[
    (gdf['time']>= datetime.combine(dateFrom, datetime.min.time())) & 
    (gdf['time']<= datetime.combine(dateTo, datetime.max.time())) &
    gdf.geometry.apply(lambda geom: polygon.contains(geom))
]



def createMap(gdf):
    mean_longitude = gdf['geometry'].x.mean()
    mean_latitude = gdf['geometry'].y.mean()
    provider = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
    m = folium.Map(tiles=provider, attr="osm", location=[mean_latitude, mean_longitude], start_zoom=2, prefer_canvas=True)
    
    mc = MarkerCluster()
    #add markers
    folium.GeoJson(
        gdf.drop(columns=['time']),
        name="Locations",
        zoom_on_click=True,
        marker=folium.Marker(),
        # tooltip=folium.GeoJsonTooltip(fields=["observed_on", "time"]),
        # popup=folium.GeoJsonPopup(fields=["observed_on", "time"]),
    ).add_to(m)
    #mc.add_to(m)

    # callback = ('function (row) {' 
    #             'var circle = L.circle(new L.LatLng(row[0], row[1]), {color: "red",  radius: 2000});'
    #             'return circle};')


    # fmc = FastMarkerCluster(data=zip(gdf.geometry.y, gdf.geometry.x), callback=callback)
    # m.add_child(fmc)
    return st_folium(m, width=1500 )

# st_data2 =createMap(gdf)

def createHeatmap(gdf):
    mean_longitude = gdf['geometry'].x.mean()
    mean_latitude = gdf['geometry'].y.mean()
    provider = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
    m2 = folium.Map(tiles=provider, attr="osm", location=[mean_latitude, mean_longitude], start_zoom=16, prefer_canvas=True)

    HeatMap(data=zip(gdf.geometry.y, gdf.geometry.x)).add_to(m2)
    return st_folium(m2, width=1500)


def creatTimedMap(gdf):
    features = []

    for index, row in gdf.iterrows():
        feature = {
            'type': 'Feature',
            'geometry': row['geometry'].__geo_interface__,
            'properties': {
                'time': row['time'].strftime('%Y-%m-%d %H:%M:%S'),  # Format timestamp as a string
                'style': {'color': 'blue'}  # You can customize the style as needed
            }
        }
        features.append(feature)

    #st.write(features)
    m4 = folium.Map(location=[0, 0], zoom_start=12)  # Specify the initial map location and zoom level
    TimestampedGeoJson(
        {'type': 'FeatureCollection', 'features': features},
        period= 'P1D',  # Time interval between timestamps
        add_last_point=True,
        auto_play=False,
        loop=False,
        transition_time=200,
        duration= 'P1W'
    ).add_to(m4)
    return st_folium(m4, width=1500, height=750)

final = creatTimedMap(gdf)

